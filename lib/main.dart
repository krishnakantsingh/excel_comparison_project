import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';

String path11 = '';
String path22 = '';
int col11 = 0;
int col22 = 0;
String text1 = '';
String text2 = '';
String _excelFilePath1 = '';
String _excelFilePath2 = '';
int _selectedNumber = 0;
int _selectedNumber2 = 0;
String _textFieldValue = '';
String _textFieldValue2 = '';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Excel File Compare',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,

      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  Future<void> _pickExcelFile1() async {
    String? filePath = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['xlsx', 'xls'],
    ).then((value) => value?.files.single.path);

    if (filePath != null) {
      setState(() {
        _excelFilePath1 = filePath;
      });
    }
  }

  Future<void> _pickExcelFile2() async {
    String? filePath = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['xlsx', 'xls'],
    ).then((value) => value?.files.single.path);

    if (filePath != null) {
      setState(() {
        _excelFilePath2 = filePath;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Excel File Compare'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [

            ElevatedButton.icon(
              onPressed: _pickExcelFile1,
              icon: Icon(Icons.add),
              label: Text('Select Excel File 1'),
              style: ElevatedButton.styleFrom(
                primary: Colors.grey, // Set button background color
                onPrimary: Colors.black, // Set button text color
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),
            SizedBox(height: 8),
            Text(_excelFilePath1),
            SizedBox(height: 16),

            TextField(
              onChanged: (value) {
                setState(() {
                  _textFieldValue = value;
                });
              },
              decoration: InputDecoration(
                hintText: 'Enter Spreadsheet 1 Name (Case Sensitive...!)',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 16),


            DropdownButton<int>(
              value: _selectedNumber != 0 ? _selectedNumber : 0,
              onChanged: (int? newValue) {
                setState(() {
                  _selectedNumber = newValue!;
                });
              },
              items: [
                DropdownMenuItem(
                  value: 0,
                  child: Text('Select a coloumn number for file 1'),
                ),
                ...List.generate(
                  50,
                      (index) => DropdownMenuItem(
                    value: index + 1,
                    child: Text('${index + 1}'),
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),


            Container(height: 50,width: 50,),

            ElevatedButton.icon(
              onPressed: _pickExcelFile2,
              icon: Icon(Icons.add),
              label: Text('Select Excel File 2'),
              style: ElevatedButton.styleFrom(
                primary: Colors.grey, // Set button background color
                onPrimary: Colors.black, // Set button text color
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),
            SizedBox(height: 8),
            Text(_excelFilePath2),
            SizedBox(height: 16),

            TextField(
              onChanged: (value) {
                setState(() {
                  _textFieldValue2 = value;
                });
              },
              decoration: InputDecoration(
                hintText: 'Enter Spreadsheet 2 Name (Case Sensitive...!)',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 16),

            DropdownButton<int>(
              value: _selectedNumber2 != 0 ? _selectedNumber2 : 0,
              onChanged: (int? newValue) {
                setState(() {
                  _selectedNumber2 = newValue!;
                });
              },
              items: [
                DropdownMenuItem(
                  value: 0,
                  child: Text('Select a Column Numberfor file 2'),
                ),
                ...List.generate(
                  50,
                      (index) => DropdownMenuItem(
                    value: index + 1,
                    child: Text('${index + 1}'),
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),
            Container(height: 30,width: 50,),


            ElevatedButton.icon(
              onPressed:() {
// Do something with the selected file paths and text field values
//                 print('Excel File 1: $_excelFilePath1');
//                 print('Excel File 2: $_excelFilePath2');
//                 print('Selected Number: $_selectedNumber');
//                 print('Selected Number 2: $_selectedNumber2');
//                 print('Text Field Value: $_textFieldValue');
//                 print('Text Field Value 2: $_textFieldValue2');

                 path11 = _excelFilePath1;
                 path22 = _excelFilePath2;
                 col11 = _selectedNumber;
                 col22 = _selectedNumber2;
                 text1 = _textFieldValue;
                 text2 = _textFieldValue2;
                Navigator.push(context, MaterialPageRoute(builder: (context)=>MyApp()));
              },
              icon: Icon(Icons.account_tree),
              label: Text('Compare'),
              style: ElevatedButton.styleFrom(
                primary: Colors.black, // Set button background color
                onPrimary: Colors.white, // Set button text color
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),





          ],
        ),
      ),
    );
  }
}
