import 'dart:io';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:excel/excel.dart';
import 'main.dart';

int countfirst = 0;
int countsecond = 0;
void main(){
  runApp(demoApp());
}
class demoApp extends  StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Practice",
      theme: ThemeData(
          primarySwatch: Colors.teal
      ),
      home: ComparedPage(),
    );
  }
}

class ComparedPage extends StatefulWidget {
  const ComparedPage({Key? key}) : super(key: key);

  @override
  State<ComparedPage> createState() => _ComparedPageState();
}

class _ComparedPageState extends State<ComparedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>MyApp())),
        ),
        title: Text("Sample"),
        centerTitle: true,
      ),
      body: Center(
        child: OutlinedButton(onPressed: createExcel,
            child: Text('Click')),
      ),

    );
  }

  Future<void> createExcel() async {
    print('Excel File 1: $path11');
    print('Excel File 2: $path22');
    print('Selected Number: $col11');
    print('Selected Number 2: $col22');
    print('Text Field Value: $text1');
    print('Text Field Value 2: $text2');
    var firstExcel = Excel.decodeBytes(
        File(path11).readAsBytesSync());
    var secondExcel = Excel.decodeBytes(
        File(path22).readAsBytesSync());
    var firstSheet = firstExcel[text1];
    var secondSheet = secondExcel[text2];

    for (var row in firstSheet.rows) {
      countfirst++;
    }
    for (var row in secondSheet.rows) {
      countsecond++;
    }

    if (countsecond < countfirst) {
      var missingRows = <List>[];

      for (var row in firstSheet.rows) {
        var found = false;
        var firstValue = row[col11];

        for (var secondRow in secondSheet.rows) {
          var secondValue = secondRow[col22];
          if (firstValue == secondValue) {
            found = true;
            break;
          }
        }

        if (found == false) {
          missingRows.add(row);
        }
      }

      var newExcel = Excel.createExcel();
      var sheet = newExcel['Sheet1'];


      for (var row in missingRows) {
        sheet.appendRow(row);
      }

      for (var row in sheet.rows) {
        for (var cell in row) {
          var value = cell?.value.toString();
          String? input = value;
          List<String>? parts = input?.split(", ");
          String output = parts![0].substring(4);
          var modifiedValue = "";
          if (output != "") {
            String out = output.replaceAll("(", "");
            modifiedValue = out;
          }
          else {
            modifiedValue = output;
          }
          cell?.value = modifiedValue;
        }
      }

      var fileBytes = newExcel.encode();
      final String path = (await getApplicationSupportDirectory()).path;
      final String fileName = '$path/Output.xlsx';
      // final String fileName = 'assets/Output.xlsx';
      final File file = File(fileName);
      file.writeAsBytesSync(fileBytes!);
      OpenFile.open(fileName);

    }
    if (countfirst < countsecond) {
      var missingRows = <List>[];

      // for (var row in firstSheet.rows) {
      for (var secondRow in secondSheet.rows) {
        var found = false;
        var firstValue = secondRow[col11];

        for (var row in firstSheet.rows) {
          var secondValue = row[col22];
          if (firstValue == secondValue) {
            found = true;
            break;
          }
        }

        if (found == false) {
          missingRows.add(secondRow);
        }
      }

      var newExcel = Excel.createExcel();
      var sheet = newExcel['Sheet1'];


      for (var row in missingRows) {
        sheet.appendRow(row);
      }

      for (var row in sheet.rows) {
        for (var cell in row) {
          var value = cell?.value.toString();
          String? input = value;
          List<String>? parts = input?.split(", ");
          String output = parts![0].substring(4);
          var modifiedValue = "";
          if (output != "") {
            String out = output.replaceAll("(", "");
            modifiedValue = out;
          }
          else {
            modifiedValue = output;
          }
          cell?.value = modifiedValue;
        }
      }

      var fileBytes = newExcel.encode();
      final String path = (await getApplicationSupportDirectory()).path;
      final String fileName = '$path/Output1.xlsx';
      // final String fileName = 'assets/Output.xlsx';
      final File file = File(fileName);
      file.writeAsBytesSync(fileBytes!);
      OpenFile.open(fileName);
    };

    if (countfirst == countsecond) {
      Future<void> showMessageDialog(String message) async {
        await showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                  title: const Text('NO MISSING ELEMENT'),
                  content: Text(message),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('OK'),
                    ),
                  ]);
            });
      }
    }
  }
}