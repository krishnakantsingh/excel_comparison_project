import 'dart:io';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:excel/excel.dart';
void main() => runApp(FinalApp());

class FinalApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Excel Reader',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  String _filePath1 = '';
  String _filePath2='';
  List<List<dynamic>> _excelData1 = [];
  List<List<dynamic>> _excelData2=[];

  Future<void> _pickExcelFile1() async {
    final result1 = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['xls', 'xlsx'],
    );
    if (result1 == null) return;

    final file1 = File(result1.files.single.path!);
    final bytes1 = await file1.readAsBytes();

    final excel1 = Excel.decodeBytes(bytes1);
    final table1 = excel1.tables[excel1.tables.keys.first]!;

    setState(() {
      _filePath1 = result1.files.single.path!;
      _excelData1 = table1.rows;
    });

    Future<void> _pickExcelFile2() async {
      final result2 = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['xls', 'xlsx'],
      );
      if (result2 == null) return;

      final file2 = File(result2.files.single.path!);
      final bytes2 = await file2.readAsBytes();

      final excel2 = Excel.decodeBytes(bytes2);
      final table2 = excel2.tables[excel2.tables.keys.first]!;

      setState(() {
        _filePath2 = result2.files.single.path!;
        _excelData2 = table2.rows;
      });

