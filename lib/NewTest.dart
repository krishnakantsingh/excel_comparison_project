import 'dart:io';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:excel/excel.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Excel Reader',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _filePath = '';
  List<List<dynamic>> _excelData = [];

  Future<void> _pickExcelFile() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['xls', 'xlsx'],
    );

    if (result == null) return;

    final file = File(result.files.single.path!);
    final bytes = await file.readAsBytes();

    final excel = Excel.decodeBytes(bytes);
    final table = excel.tables[excel.tables.keys.first]!;

    setState(() {
      _filePath = result.files.single.path!;
      _excelData = table.rows;
    });

    for (var row in table.rows) {
      debugPrint(row.join(', '));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Excel Reader'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: _pickExcelFile,
              child: Text('Pick an Excel file'),
            ),
            SizedBox(height: 20),
            Text('File path: $_filePath'),
            SizedBox(height: 20),
            if (_excelData.isNotEmpty)
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                    columns: _excelData.first
                        .map((header) => DataColumn(label: Text(header.toString())))
                        .toList(),
                    rows: _excelData.skip(1)
                        .map((row) => DataRow(cells: row.map((cell) => DataCell(Text(cell.toString()))).toList()))
                        .toList(),
                  ),
                ),
              ),
            if (_excelData.isEmpty)
              Text('No data to display'),
          ],
        ),
      ),
    );
  }
}